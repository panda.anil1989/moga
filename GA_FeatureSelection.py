from GeneticAlgorithm import GeneticSelector


import random
import numpy as np


from sklearn.datasets import load_breast_cancer
from sklearn.model_selection import cross_val_score
from sklearn.linear_model import LinearRegression,LogisticRegression

from sklearn.feature_selection import RFECV
from sklearn.ensemble import RandomForestClassifier



SEED = 2018
random.seed(SEED)
np.random.seed(SEED)

import warnings
warnings.filterwarnings("ignore")


#==============================================================================
# Data
#==============================================================================

dataset = load_breast_cancer()
X, y = dataset.data, dataset.target
features = dataset.feature_names

#==============================================================================
# CV MSE before feature selection
#==============================================================================
est = LogisticRegression()
score = 1.0 * cross_val_score(est, X, y, cv=5, scoring="r2")
print("CV MSE before feature selection: {:.2f}".format(np.mean(score)))

#==============================================================================
# CV MSE after feature selection: RFE
#==============================================================================
rfe = RFECV(est, cv=5, scoring="r2")
rfe.fit(X, y)
score = 1.0 * cross_val_score(est, X[:,rfe.support_], y, cv=5, scoring="r2")
print("CV MSE after RFE feature selection: {:.2f}".format(np.mean(score)))

#==============================================================================
# CV MSE after feature selection: Feature Importance
#==============================================================================
rf = RandomForestClassifier(n_estimators=500, random_state=SEED)
rf.fit(X, y)
support = rf.feature_importances_ > 0.01
score = 1.0 * cross_val_score(est, X[:,support], y, cv=5, scoring="r2")
print("CV MSE after Feature Importance feature selection: {:.2f}".format(np.mean(score)))

#==============================================================================
# CV MSE after feature selection: Genetic Feature Selector
#==============================================================================

sel = GeneticSelector(estimator=LogisticRegression(),
                      n_gen=7, size=200, n_best=40, n_rand=40,
                      n_children=5, mutation_rate=0.05)
sel.fit(X,y)

print(sel.support_)
est=RandomForestClassifier()
score = 1.0 * cross_val_score(est, X[:,sel.support_], y, cv=5, scoring="r2")
print("CV MSE after GA+LR feature selection: {:.2f}".format(np.mean(score)))